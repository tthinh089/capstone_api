function Product(
  _id,
  _name,
  _price,
  _screen,
  _backCamera,
  _frontCamera,
  _img,
  _desc,
  _type
) {
  this.id = _id;
  this.name = _name;
  this.price = _price;
  this.screen = _screen;
  this.backCamera = _backCamera;
  this.frontCamera = _frontCamera;
  this.img = _img;
  this.desc = _desc;
  this.type = _type;
}

class itemCart {
  constructor(_id, _name, _price, _quantity, _img) {
    this.id = _id;
    this.name = _name;
    this.price = _price;
    this.quantity = _quantity;
    this.img = _img;
  }
  getQuantity() {
    console.log(this.quantity);
  }
}
