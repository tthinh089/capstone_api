var cartLocal = JSON.parse(localStorage.getItem("productList"));
if (cartLocal === null) {
  cartLocal = [];
}
let saveData = (data) => {
  localStorage.setItem("productList", JSON.stringify(data));
};
const fetchData = async () => {
  try {
    const getProduct = await axios({
      url: "https://64537c8cc18adbbdfe9e88e0.mockapi.io/phone",
      method: "GET",
    });
    return getProduct.data;
  } catch (error) {
    console.log(error);
  }
};

renderProduct = (productArr) => {
  var contentHTML = "";
  for (i = 0; i < productArr.length; i++) {
    var product = productArr[i];
    var productContent = `
        <div class="product-container">
        <img
          src="${product.img}"
          alt="Product Image"
        />
        <div class="product-content">
          <div class="info-top">
            <h2 class="product-name">${product.name}</h2>
            <p class="product-price">${product.price}$</p>
          </div>
          <div class="info-bottom">
            <h5>Screen: ${product.screen}</h5>
            <h5>Back-Camera: ${product.backCamera}</h5>
            <h5>Front-Camera: ${product.frontCamera}</h5>
            <p class="product-description">Desc: ${product.desc}</p>
            </div>
            <button onclick="addProductCart(${product.id})">Add to Cart</button>
        </div>
      </div>
    `;
    contentHTML += productContent;
    document.querySelector(".products").innerHTML = contentHTML;
  }
};

Sorting = () => {
  const UIChange = fetchData().then((res) => {
    let valueUI = document.getElementById("productType").value;
    const typeArr = res.filter((value) => {
      if (value.type === valueUI) {
        return value;
      } else if (valueUI == "all") {
        value = res;
        return value;
      }
    });
    contentHTML = ``;
    for (i = 0; i < typeArr.length; i++) {
      var product = typeArr[i];
      contentHTML += `
        <div class="product-container">
        <img
          src="${product.img}"
          alt="Product Image"
        />
        <div class="product-content">
          <div class="info-top">
            <h2 class="product-name">${product.name}</h2>
            <p class="product-price">${product.price}$</p>
          </div>
          <div class="info-bottom">
            <h5>Screen: ${product.screen}</h5>
            <h5>Back-Camera: ${product.backCamera}</h5>
            <h5>Front-Camera: ${product.frontCamera}</h5>
            <p class="product-description">Desc: ${product.desc}</p>
            </div>
            <button onclick="addProductCart(${product.id})">Add to Cart</button>
        </div>
      </div>
      `;
    }
    document.querySelector(".products").innerHTML = contentHTML;
  });
};

function getProductInJSON() {
  let cartLocalS = JSON.parse(localStorage.getItem("productList"));
  let contentR = "";
  let finalPrice = 0;
  for (var i = 0; i < cartLocalS.length; i++) {
    let product = cartLocalS[i];
    finalPrice += product.price * product.quantity;
    contentR += `
              <tr>
                <td>${i + 1}</td>
                <td>
                <img src="${product.img}" width="75px" />
                </td>
                <td>${product.name}</td>
                <td>
                <button type="button" class="btn btn-warning" id="decrease-${
                  product.id
                }" onclick="decreaseP(${product.id})" ${
      product.quantity === 1 ? "disabled" : ""
    }>-</button>
                ${product.quantity}
                <button type="button" class="btn btn-warning" onclick="increaseP(${
                  product.id
                })">+</button>
                </td>
                <td>${product.price}</td>
                <td>
                <button type="button" class="btn btn-warning" onclick="remove(${
                  product.id
                })">Remove</button>
                </td>
              </tr>
          `;
  }
  document.getElementById(
    "finalPrice"
  ).innerHTML = `Final price: ${finalPrice} $`;
  document.getElementById("showCart").innerHTML = contentR;
}

addProductCart = (id) => {
  const getItemwithID = async (id) => {
    try {
      const getProductID = await axios({
        url: `https://64537c8cc18adbbdfe9e88e0.mockapi.io/phone/${id}`,
        method: "GET",
      });
      Toastify({
        text: "Add product to cart successfully!",
        className: "info",
        close: true,
        gravity: "bottom", // `top` or `bottom`
        position: "right", // `left`, `center` or `right`
        style: {
          background: "linear-gradient(to right, #00b09b, #96c93d)",
        },
      }).showToast();
      return getProductID.data;
    } catch (error) {
      console.log(error);
    }
  };
  const itemByID = getItemwithID(id).then((res) => {
    let itemNew = new itemCart(res.id, res.name, res.price, 1, res.img);
    let index = cartLocal.findIndex((item) => item.id === itemNew.id);
    if (index === -1) {
      cartLocal = [...cartLocal, itemNew];
    } else {
      cartLocal[index].quantity++;
    }
    saveData(cartLocal);
    function renderCart(cartLocal) {
      let contentR = "";
      for (var i = 0; i < cartLocal.length; i++) {
        let product = cartLocal[i];
        contentR += `
              <tr>
                <td>${i + 1}</td>
                <td>
                <img src="${product.img}" width="75px" />
                </td>
                <td>${product.name}</td>
                <td>
                <button type="button" class="btn btn-warning" id="decrease-${
                  product.id
                }" onclick="decreaseP(${product.id})" ${
          product.quantity === 1 ? "disabled" : ""
        }>-</button>
                ${product.quantity}
                <button type="button" class="btn btn-warning" onclick="increaseP(${
                  product.id
                })">+</button>
                </td>
                <td>${product.price}</td>
                <td>
                <button type="button" class="btn btn-warning" onclick="remove(${
                  product.id
                })">Remove</button>
                </td>
              </tr>                    
          `;
      }
      document.getElementById("showCart").innerHTML = contentR;
    }
    renderCart(cartLocal);
    console.log(cartLocal);
  });
};
function remove(id) {
  let idProduct = id;
  cartLocal = cartLocal.filter((item) => item.id != idProduct);
  saveData(cartLocal);
  Toastify({
    text: "Remove product from cart successfully!",
    className: "info",
    close: true,
    gravity: "bottom", // `top` or `bottom`
    position: "right", // `left`, `center` or `right`
    style: {
      background: "linear-gradient(to right, #09792f, #96c93d)",
    },
  }).showToast();
  function Re_renderCart(cartLocal) {
    let contentR = "";
    let finalPrice = 0;
    for (var i = 0; i < cartLocal.length; i++) {
      let product = cartLocal[i];
      let total = product.price * product.quantity;
      finalPrice += total;
      contentR += `
            <tr>
              <td>${i + 1}</td>
              <td>
              <img src="${product.img}" width="75px" />
              </td>
              <td>${product.name}</td>
              <td>
              <button type="button" class="btn btn-warning" id="decrease-${
                product.id
              }" onclick="decreaseP(${product.id})" ${
        product.quantity === 1 ? "disabled" : ""
      }>-</button>
              ${product.quantity}
              <button type="button" class="btn btn-warning" onclick="increaseP(${
                product.id
              })">+</button>
              </td>
              <td>${total}</td>
              <td>
              <button type="button" class="btn btn-warning" onclick="remove(${
                product.id
              })">Remove</button>
              </td>
            </tr>                    
        `;
    }
    document.getElementById(
      "finalPrice"
    ).innerHTML = `Final price: ${finalPrice} $`;
    document.getElementById("showCart").innerHTML = contentR;
  }
  Re_renderCart(cartLocal);
}
// ----------------------------------------------------------------------------------------------------
function increaseP(id) {
  let idProduct = id;
  for (var i = 0; i <= cartLocal.length; i++) {
    let idProductInArr = cartLocal[i]?.id;
    let idParse = Number(idProductInArr);
    if (idProduct === idParse) {
      cartLocal[i].quantity++;
    }
    console.log(cartLocal);
  }
  saveData(cartLocal);
  function Re_renderCart(cartLocal) {
    let contentR = "";
    let finalPrice = 0;
    for (var i = 0; i < cartLocal.length; i++) {
      let product = cartLocal[i];
      let total = product.price * product.quantity;
      finalPrice += total;
      contentR += `
            <tr>
              <td>${i + 1}</td>
              <td>
              <img src="${product.img}" width="75px" />
              </td>
              <td>${product.name}</td>
              <td>
              <button type="button" class="btn btn-warning" id="decrease-${
                product.id
              }" onclick="decreaseP(${product.id})" ${
        product.quantity === 1 ? "disabled" : ""
      }>-</button>
              ${product.quantity}
              <button type="button" class="btn btn-warning" onclick="increaseP(${
                product.id
              })">+</button>
              </td>
              <td>${total}</td>
              <td>
              <button type="button" class="btn btn-warning" onclick="remove(${
                product.id
              })">Remove</button>
              </td>
            </tr>                    
        `;
    }
    document.getElementById(
      "finalPrice"
    ).innerHTML = `Final price: ${finalPrice} $`;
    document.getElementById("showCart").innerHTML = contentR;
  }
  Re_renderCart(cartLocal);
}

function decreaseP(id) {
  let idProduct = id;
  for (var i = 0; i <= cartLocal.length; i++) {
    let idProductInArr = cartLocal[i]?.id;
    let idParse = Number(idProductInArr);
    if (idProduct === idParse) {
      cartLocal[i].quantity--;
    }
  }
  saveData(cartLocal);
  function Re_renderCart(cartLocal) {
    let contentR = "";
    let finalPrice = 0;
    for (var i = 0; i < cartLocal.length; i++) {
      let product = cartLocal[i];
      let total = product.price * product.quantity;
      finalPrice += total;
      contentR += `
            <tr>
              <td>${i + 1}</td>
              <td>
              <img src="${product.img}" width="75px" />
              </td>
              <td>${product.name}</td>
              <td>
              <button type="button" class="btn btn-warning" id="decrease-${
                product.id
              }" onclick="decreaseP(${product.id})" ${
        product.quantity === 1 ? "disabled" : ""
      }>-</button>
              ${product.quantity}
              <button type="button" class="btn btn-warning" onclick="increaseP(${
                product.id
              })">+</button>
              </td>
              <td>${total}</td>
              <td>
              <button type="button" class="btn btn-warning" onclick="remove(${
                product.id
              })">Remove</button>
              </td>
            </tr>                    
        `;
    }
    document.getElementById(
      "finalPrice"
    ).innerHTML = `Final price: ${finalPrice}$`;
    document.getElementById("showCart").innerHTML = contentR;
  }
  Re_renderCart(cartLocal);
}

function btnMua() {
  if (cartLocal == "") {
    alert("You don't have any product to purchase!");
  } else {
    cartLocal = [];
    saveData(cartLocal);
    let contentR = "";
    let finalPrice = 0;
    for (var i = 0; i < cartLocal.length; i++) {
      let product = cartLocal[i];
      let total = product.price * product.quantity;
      contentR += `
              <tr>
                <td>${i + 1}</td>
                <td>
                <img src="${product.img}" width="75px" />
                </td>
                <td>${product.name}</td>
                <td>
                <button type="button" class="btn btn-warning" id="decrease-${
                  product.id
                }" onclick="decreaseP(${product.id})" ${
        product.quantity === 1 ? "disabled" : ""
      }>-</button>
                ${product.quantity}
                <button type="button" class="btn btn-warning" onclick="increaseP(${
                  product.id
                })">+</button>
                </td>
                <td>${total}</td>
                <td>
                <button type="button" class="btn btn-warning" onclick="remove(${
                  product.id
                })">Remove</button>
                </td>
              </tr>                    
          `;
    }
    document.getElementById(
      "finalPrice"
    ).innerHTML = `Final price: ${finalPrice}$`;
    document.getElementById("showCart").innerHTML = contentR;
    Toastify({
      text: "Purchased product(s) successfully!",
      className: "info",
      close: true,
      gravity: "bottom", // `top` or `bottom`
      position: "right", // `left`, `center` or `right`
      style: {
        background: "linear-gradient(to right, #00b09b, #96c93d)",
      },
    }).showToast();
  }
}

function btnXoa() {
  cartLocal = [];
  saveData(cartLocal);
  let contentR = "";
  let finalPrice = 0;
  for (var i = 0; i < cartLocal.length; i++) {
    let product = cartLocal[i];
    let total = product.price * product.quantity;
    contentR += `
            <tr>
              <td>${i + 1}</td>
              <td>
              <img src="${product.img}" width="75px" />
              </td>
              <td>${product.name}</td>
              <td>
              <button type="button" class="btn btn-warning" id="decrease-${
                product.id
              }" onclick="decreaseP(${product.id})" ${
      product.quantity === 1 ? "disabled" : ""
    }>-</button>
              ${product.quantity}
              <button type="button" class="btn btn-warning" onclick="increaseP(${
                product.id
              })">+</button>
              </td>
              <td>${total}</td>
              <td>
              <button type="button" class="btn btn-warning" onclick="remove(${
                product.id
              })">Remove</button>
              </td>
            </tr>                    
        `;
  }
  document.getElementById(
    "finalPrice"
  ).innerHTML = `Final price: ${finalPrice}$`;
  document.getElementById("showCart").innerHTML = contentR;
}
