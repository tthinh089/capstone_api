var servicesPhone = {
  // gọi API để lấy danh sách sản phẩm
  getList: function () {
    return axios({
      url: URL_BASE + `/phone`,
      method: "GET",
    });
  },
  // GOIJ api để thêm product vào danh sách
  addList: function (product) {
    return axios({
      url: URL_BASE + `/phone`,
      method: "POST",
      data: product,
    });
  },
  deleteList: function (id) {
    return axios({
      url: URL_BASE + `/phone/${id}`,
      method: "DELETE",
    });
  },
  getListFormId: function (id) {
    return axios({
      url: URL_BASE + `/phone/${id}`,
      method: "GET",
    });
  },
  //   goij API update danh sasch qua id, cập nhập data mới
  updateListFormId: function (id) {
    return axios({
      url: URL_BASE + `/phone/${id}`,
      method: "PUT",
      data: layThongTinTuForm(),
    });
  },
};
