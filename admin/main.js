const URL_BASE = `https://64537c8cc18adbbdfe9e88e0.mockapi.io`;
// var idSelector = null;
function fecthListPhone() {
  batLoading();
  servicesPhone
    .getList()
    .then(function (res) {
      tatLoading();
      console.log("res: ", res);
      renderListPhone(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}
fecthListPhone();
function addPhone() {
  var phone = layThongTinTuForm();
  if (checkValidate()) {
    batLoading();
    servicesPhone
      .addList(phone)
      .then(function (res) {
        resetData();
        $("#exampleModal").modal("hide");
        $(".modal-backdrop").remove();
        console.log("res: ", res);
        fecthListPhone();
      })
      .catch(function (err) {
        tatLoading();
        console.log("err: ", err);
      });
  }
}
function deleteProduct(id) {
  batLoading();
  servicesPhone
    .deleteList(id)
    .then(function (res) {
      console.log("res: ", res);
      fecthListPhone();
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}
function editProduct(id) {
  idSelector = id;
  batLoading();
  servicesPhone
    .getListFormId(id)
    .then(function (res) {
      tatLoading();
      resetData();
      console.log("res: ", res);
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}
function updatePhone() {
  if (checkValidate()) {
    batLoading();
    servicesPhone
      .updateListFormId(idSelector)
      .then(function (res) {
        $("#exampleModal").modal("hide");

        $(".modal-backdrop").remove();
        resetData();
        console.log("res: ", res);
        fecthListPhone();
      })
      .catch(function (err) {
        tatLoading();
        console.log("err: ", err);
      });
  }
}

function btnSearch() {
  batLoading();
  servicesPhone
    .getList()
    .then(function (res) {
      tatLoading();
      var phoneArr = res.data;
      var searchList = searchProduct(phoneArr);
      renderListPhone(searchList);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}

function sortReduce() {
  batLoading();
  servicesPhone
    .getList()
    .then(function (res) {
      tatLoading();
      var arrPhone = res.data;
      arrPhone = sortPriceLowToHight(arrPhone);
      renderListPhone(arrPhone);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}

function sortIncrease() {
  batLoading();
  servicesPhone
    .getList()
    .then(function (res) {
      tatLoading();
      var arrPhone = res.data;
      arrPhone = sortPriceHightToLow(arrPhone);
      renderListPhone(arrPhone);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}
function closeModal() {
  resetData();
}
function openModal() {
  resetData();
}
function closeStick() {
  resetData();

}
/**
 * Samsung Galaxy S23 Ultra
 * $1500
 * Smartphone Galaxy đầu tiên có cảm biến siêu phân giải 200MP
 * 200.0 MP + 12.0 MP
 * 10.0 MP + 10.0 MP
 * https://product.hstatic.net/200000670897/product/9_2a06f2d108b74586a843b8cb8c5177a9_master.jpg
 * Vẻ đẹp từ sắc màu thiên nhiên tinh tế
 */

/**iPhone 14 Pro Max
 * 2000
 * Màn hình Dynamic Island
 * * 48MP + 12MP + 12MP
 * 12MP + 12MP + 12MP
 * https://cdn1.hoanghamobile.com/tin-tuc/wp-content/uploads/2022/10/iphone-14-plus-chong-va-dap-tot-hon-iphone-14-pro-max-2.jpg
 * Cấu hình iPhone 14 Pro Max mạnh mẽ, hiệu năng cực khủng từ chipset A16 Bionic
 
*/
