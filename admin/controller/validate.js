function messErr (idReport, mess) {
    document.getElementById(idReport).innerHTML = `<p>${mess}</p>`
}


function testEmty(idEL, value) {
    if((value.length == 0) || (value = 0)) {
         messErr(idEL,"Trường này không được bỏ trống" );
         return false;
    } else {
         messErr(idEL,"" );
        return true;
    }
}
function testNumber(idEL, value) {
    if (isNaN(value)) {
        messErr(idEL, "Không phải kiểu dữ liệu Number");
        return false;
       
    }else if(value < 0){
        messErr(idEL, "Gía không hợp lệ")
    }
     else{
         messErr(idEL, "");
        return true;
    }
}

function testURL(idEL, value) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  if (pattern.test(value)) {
    messErr(idEL,"" );
        return true;
  } else {
    messErr(idEL, "String is not URL");
        return false;
  }
}