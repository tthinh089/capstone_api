var layThongTinTuForm = function () {
  var id = null;
  var name = document.getElementById("name").value;
  var price = document.getElementById("price").value * 1;
  var screen = document.getElementById("screen").value;
  var backCamera = document.getElementById("back").value;
  var frontCamera = document.getElementById("font").value;
  var img = document.getElementById("img").value;
  var desc = document.getElementById("desc").value;
  var brand = document.getElementById("type");
  var type = brand.options[brand.selectedIndex].text;
  var phone = new phoneCyber(
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type
  );
  return phone;
};
function renderListPhone(arrList) {
  var contentHTML = ``;
  for (var i = 0; i < arrList.length; i++) {
    var phone = arrList[i];
    var contentRow = ``;
    contentRow = `
        <tr>
        <td>${phone.id}</td>
        <td>${phone.name}</td>
        <td>${phone.price}</td>
        <td><img src="${phone.img}" class="img-fluid" alt="Responsive image" /></td>
        <td>${phone.desc}</td>
        <td><button class= "btn btn-danger" onclick ="deleteProduct('${phone.id}')">Delete <i class="fa fa-trash"></i></button></td>
        <td><button class= "btn btn-primary"  data-toggle="modal"
            data-target="#exampleModal" onclick ="editProduct('${phone.id}')">Edit <i class="fa fa-edit"></i></button></td>
        
        </tr>`;
    contentHTML += contentRow;
  }
  document.getElementById("tablePhone").innerHTML = contentHTML;
}
function showThongTinLenForm(phone) {
  document.getElementById("name").value = phone.name;
  document.getElementById("price").value = phone.price;
  document.getElementById("screen").value = phone.screen;
  document.getElementById("back").value = phone.backCamera;
  document.getElementById("font").value = phone.frontCamera;
  document.getElementById("img").value = phone.img;
  document.getElementById("desc").value = phone.desc;
  var Namebrand = document.getElementById("type");
  var brand = Namebrand.getElementsByTagName("option");
  for (var i = 0; i < brand.length; i++) {
    if (phone.type.toLowerCase() === brand[i].innerText.toLocaleLowerCase()) {
      brand[i].selected = true;
      console.log("brand[i].selected: ", brand[i].selected);
      break;
    }
  }
  // var brand = document.getElementById("type");
  // brand.options[brand.selectedIndex].text = phone.type;
}
// validate
function checkValidate() {
  var dataPhone = layThongTinTuForm();
  var brand = document.getElementById("type").value;
  // test name
  var isValue = testEmty("tb-name", dataPhone.name);
  // test price
  isValue =
    isValue &
    (testEmty("tb-price", dataPhone.price) &&
      testNumber("tb-price", dataPhone.price));
  // test screen
  isValue = isValue & testEmty("tb-screen", dataPhone.screen);
  // test back camera
  isValue = isValue & testEmty("tb-backCamera", dataPhone.backCamera);
  // test front camera
  isValue = isValue & testEmty("tb-frontCamera", dataPhone.frontCamera);
  // test front camera
  isValue = isValue & testEmty("tb-frontCamera", dataPhone.frontCamera);
  // test img
  isValue =
    isValue &
    (testEmty("tb-img", dataPhone.img) && testURL("tb-img", dataPhone.img));
  // test desc
  isValue = isValue & testEmty("tb-desc", dataPhone.desc);

  // // test type
  isValue = isValue & testEmty("tb-type", brand);
  return isValue;
}

// search
function searchProduct(phoneArr) {
  var key = document.getElementById("searchText").value;
  var arrTem = [];
  key = key.toLowerCase();
  for (var i = 0; i < phoneArr.length; i++) {
    var name = phoneArr[i].name.toLowerCase();
    if (name.search(key) != -1) {
      arrTem.push(phoneArr[i]);
    }
  }
  return arrTem;
}

function sortPriceLowToHight(phoneArr) {
  for (var i = 0; i < phoneArr.length; i++) {
    var phone = {...phoneArr[i]};
    var min = phone.price;

    for (var j = i + 1; j < phoneArr.length; j++) {
      if (min > phoneArr[j].price) {
        var tem= {...phoneArr[i]} ;
        phoneArr[i] = {...phoneArr[j]};
        phoneArr[j] = {...tem};
        min = phoneArr[i].price;
      }
    }
  }
  return phoneArr;
}
function sortPriceHightToLow(phoneArr) {
   for (var i = 0; i < phoneArr.length; i++) {
    var phone = {...phoneArr[i]};
    var max = phone.price;

    for (var j = i + 1; j < phoneArr.length; j++) {
      if (max < phoneArr[j].price) {
        var tem= {...phoneArr[i]} ;
        phoneArr[i] = {...phoneArr[j]};
        phoneArr[j] = {...tem};
        max = phoneArr[i].price;
      }
    }
  }
  return phoneArr;
}

function batLoading() {
  document.getElementById("loading").style.display= "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display= "none";
}

function resetData() {
  document.getElementById("name").value = ""; 
  document.getElementById("price").value = ""; 
  document.getElementById("screen").value = ""; 
  document.getElementById("back").value = ""; 
  document.getElementById("font").value = ""; 
  document.getElementById("img").value = "";
  document.getElementById("desc").value = ""; 
  document.getElementById("type").value= "";
 var arr =  document.querySelectorAll(".sp-thongbao");

 for(var i = 0; i < arr.length; i++) {
  arr[i].innerHTML= "";
 }
}